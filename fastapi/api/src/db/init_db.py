from sqlalchemy.ext.declarative import declarative_base
from sqlmodel import Session, create_engine
from sqlmodel.sql.expression import Select, SelectOfScalar

from src.core.config import settings

SelectOfScalar.inherit_cache = True  # type: ignore
Select.inherit_cache = True  # type: ignore

# create the declarative base
Base = declarative_base()


DATABASE_URL = settings.DATABASE_URL

engine = create_engine(DATABASE_URL)


def get_session():
    with Session(engine) as session:
        yield session
