from pydantic import BaseSettings


class Settings(BaseSettings):
    DB_USER: str = "user"
    DB_PASSWORD: str = "password"
    DB_SCHEMA: str = "postgresql"
    DB_NAME: str = "db"
    DB_PORT: int = 5432
    DB_ADDR: str = "db"
    ROOT_PATH: str = "/"

    class Config:
        case_sensitive = True

    @property
    def DATABASE_URL(self) -> str:
        return f"{self.DB_SCHEMA}://{self.DB_USER}:{self.DB_PASSWORD}@{self.DB_ADDR}:{self.DB_PORT}/{self.DB_NAME}"


settings = Settings()
