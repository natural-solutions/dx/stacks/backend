from fastapi import APIRouter

from src.api.v1 import model

api_router = APIRouter()
api_router.include_router(model.router, prefix="/model", tags=["model"])
