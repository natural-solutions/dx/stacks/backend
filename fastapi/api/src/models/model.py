from typing import List, Optional

from sqlmodel import Field, SQLModel

from src.models.paginated import PaginatedModel


class ModelBase(SQLModel):
    name: str


class Model(SQLModel, table=True):
    id: Optional[int] = Field(primary_key=True, index=True)


class ModelRead(ModelBase):
    id: int


class ModelCreate(ModelBase):
    pass


class ModelReadAll(PaginatedModel):
    records: List[ModelRead]
