from typing import List, Optional

from pydantic import BaseModel
from sqlmodel import SQLModel


class MetaDataModel(BaseModel):
    limit: int
    previous: Optional[str]
    next: Optional[str]
    total: int


class PaginatedModel(BaseModel):
    records: List[SQLModel]
    metadata: MetaDataModel
