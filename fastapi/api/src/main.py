from fastapi import FastAPI

from src.api.v1.api import api_router
from src.core.config import settings

app = FastAPI(
    root_path=settings.ROOT_PATH,
    swagger_ui_parameters={"persistAuthorization": True},
)
app.include_router(api_router)


@app.on_event("startup")
def on_startup():
    pass
