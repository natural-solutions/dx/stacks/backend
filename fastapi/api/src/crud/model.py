from src.crud.base import CRUDBase
from src.models.model import Model, ModelCreate


class CRUDModel(CRUDBase):
    pass


crud = CRUDModel(model=Model, create_model=ModelCreate)
