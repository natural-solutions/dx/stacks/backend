from typing import List, Optional

from fastapi.encoders import jsonable_encoder
from sqlalchemy.exc import IntegrityError
from sqlmodel import SQLModel, func, select
from sqlmodel.orm.session import Session as ORMSession

from src.crud.exceptions import UpdateModelNotDefined
from src.models.paginated import PaginatedModel


def format_all_response(
    count: int, limit: int, previous_page: int, next_page: int, objects: List[SQLModel]
) -> PaginatedModel:
    """Writes a dict response containing the results (records)
    the number of total objects in the database, the page (offset)
    and the limit entered by the user. It is based on the following
    arguments

    Args:
        count (int): total number of objects in the database
        limit (int): the limit provided by the user
        offset (int): the offset (page) provided by the user
        objects (list): results of the get_all query

    Returns:
        _type_: _description_
    """
    return {
        "records": objects,
        "metadata": {"limit": limit, "previous": previous_page, "next": next_page, "total": count},
    }


class CRUDBase:
    model: SQLModel
    create_model: Optional[SQLModel]
    update_model: Optional[SQLModel]

    def __init__(
        self, model: SQLModel, create_model: SQLModel = None, update_model: SQLModel = None
    ) -> None:
        self.model = model
        self.create_model = create_model
        self.update_model = update_model

    def count(self, db: ORMSession):
        return db.query(func.count(self.model.id)).scalar()

    def get(self, db: ORMSession, obj_id: int):
        result = db.get(self.model, obj_id)
        return result

    def get_all(self, db: ORMSession, query_string="", initial_statement=None):
        if initial_statement is None:
            statement = db.query(self.model)
        else:
            statement = initial_statement
        query = statement.rql(query_string)

        limit = query._rql_compat_limit if not query._rql_limit_bypass else 0
        results, previous_page, next_page, total = query.rql_paginate()
        return format_all_response(
            count=total,
            limit=limit,
            previous_page=previous_page,
            next_page=next_page,
            objects=results,
        )

    def create(self, db: ORMSession, **kwargs):
        """Create an object in database based on the provided model and
        create_model
        **kwargs are passed to the create_model SQLModel²

        Args:
            db (ORMSession): database sesstion

        Returns:
            SQLModel: model in databse
        """
        instance = self.create_model(**kwargs)
        db_obj = self.model.from_orm(instance)
        self.add_to_db(db=db, db_obj=db_obj)
        return db_obj

    def add_to_db(self, db: ORMSession, db_obj: SQLModel):
        # TODO a try/except => Rollback ?
        db.add(db_obj)
        try:
            db.commit()
        except IntegrityError:
            db.rollback()
            raise
        db.refresh(db_obj)

    # TODO: really useful?
    def generate(self, obj: SQLModel, instance: SQLModel):
        return obj.from_orm(instance)

    def update(self, db: ORMSession, db_obj: SQLModel, obj_in: SQLModel) -> SQLModel:
        db_obj = self.prepare_update(db_obj=db_obj, obj_in=obj_in)
        self.add_to_db(db=db, db_obj=db_obj)
        return db_obj

    def prepare_update(self, db_obj: SQLModel, obj_in: SQLModel):
        if self.update_model is None:
            raise UpdateModelNotDefined(
                "You need to define update_model at crud initialisation to update a model"
            )
        obj_data = jsonable_encoder(db_obj)
        update_data = obj_in.dict(exclude_defaults=True)
        for field in obj_data:
            if field in update_data:
                setattr(db_obj, field, update_data[field])
        return db_obj

    def delete(self, db: ORMSession, db_id: int):
        with db as session:
            statement = select(self.model).where(self.model.id == db_id)
            obj = session.exec(statement).one()
            session.delete(obj)
            session.commit()
        # Cannot return obj that have relationships so no return obj here
